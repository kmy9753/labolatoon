﻿using UnityEngine;
using System.Collections;

public class PaintablePlaneManager : MonoBehaviour {

	private int row;
	private int col;
	
	public Transform floor;
	public GameObject [,] planes;
	
	public GameObject plane;
	public TextureManager textureManager;
	
	public int numPPlane;
	
	// Use this for initialization
	void Start () {
		
		transform.position = new Vector3(0f, 0f, 0f);
		transform.rotation = Quaternion.identity;
		
		row = (int)(floor.lossyScale.z / plane.transform.lossyScale.z);
		col = (int)(floor.lossyScale.x / plane.transform.lossyScale.x);
		
		planes = new GameObject[row, col];
	
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
			
				planes[i, j] = plane;
				
				planes[i, j].GetComponent<PaintablePlane>().row = i;
				planes[i, j].GetComponent<PaintablePlane>().col = j;
				
				initPlane(planes[i, j]);
				
				planes[i, j] = 
					(GameObject)Instantiate((GameObject)planes[i, j],
					                        new Vector3(
											  j * plane.transform.lossyScale.x - floor.lossyScale.x / 2f + plane.transform.lossyScale.x / 2f,
				 							  0f + 0.01f,
											  i * plane.transform.lossyScale.z - floor.lossyScale.z / 2f + plane.transform.lossyScale.z / 2f
				            				), Quaternion.identity
				            			   );
			
				planes[i, j].transform.parent = transform;
			}
		}
		
		transform.rotation = floor.rotation;
		transform.position = floor.position;// + new Vector3(-floor.transform.lossyScale.x / 2f + plane.transform.lossyScale.x / 2f, 0.01f, - floor.transform.lossyScale.z / 2f + plane.transform.lossyScale.z / 2f);
	}
	
	void initPlane(GameObject obj){
	
		PaintablePlane pPlane = obj.GetComponent<PaintablePlane>();
		
		pPlane.paintingID = PaintablePlane.PaintingID.None;
		pPlane.manager = this.gameObject;
		obj.GetComponent<Renderer>().enabled = false;
				
	}
	
	public void Paint(GameObject obj, PaintablePlane.PaintingID paintingID){
		
		PaintablePlane pPlane = obj.GetComponent<PaintablePlane>();
		
		int r = pPlane.row;
		int c = pPlane.col;
		
		// painted && same painter
		if(pPlane.paintingID == paintingID) return;
		
		pPlane.paintingID = paintingID;
		obj.GetComponent<Renderer>().enabled = true;
		
		bool faceA = (r + 1 <  row && Repaint(planes[r + 1, c    ], paintingID));
		bool faceB = (c + 1 <  col && Repaint(planes[r    , c + 1], paintingID));
		bool faceC = (r - 1 >= 0   && Repaint(planes[r - 1, c    ], paintingID));
		bool faceD = (c - 1 >= 0   && Repaint(planes[r    , c - 1], paintingID));
		
		obj.GetComponent<Renderer>().material.mainTexture = 
			textureManager.GetPaintablePlane(faceA, faceB, faceC, faceD);
		
		if(pPlane.paintingID == PaintablePlane.PaintingID.Enemy){
			obj.GetComponent<Renderer>().material.color = Color.black;
		}
		else {
			obj.GetComponent<Renderer>().material.color = Color.white;
		}
		
	}
	
	// @return obj.paintingID == paintingID
	private bool Repaint(GameObject obj, PaintablePlane.PaintingID paintingID){
		
		PaintablePlane pPlane = obj.GetComponent<PaintablePlane>();
		
		int r = pPlane.row;
		int c = pPlane.col;
		
		// !painted
		if(pPlane.paintingID == PaintablePlane.PaintingID.None) return false;
		
		bool faceA = (r + 1 <  row && planes[r + 1, c    ].GetComponent<PaintablePlane>().paintingID == pPlane.paintingID);
		bool faceB = (c + 1 <  col && planes[r    , c + 1].GetComponent<PaintablePlane>().paintingID == pPlane.paintingID);
		bool faceC = (r - 1 >= 0   && planes[r - 1, c    ].GetComponent<PaintablePlane>().paintingID == pPlane.paintingID);
		bool faceD = (c - 1 >= 0   && planes[r    , c - 1].GetComponent<PaintablePlane>().paintingID == pPlane.paintingID);
		
		obj.GetComponent<Renderer>().material.mainTexture = 
			textureManager.GetPaintablePlane(faceA, faceB, faceC, faceD);
		
		return pPlane.paintingID == paintingID;
		
	}
	
	public float CalculatePaintedRate(PaintablePlane.PaintingID paintingID) {
	
		int numPainted = 0;
		
		for(int i = 0; i < row; i++){
			for(int j = 0; j < col; j++){
				if(planes[i, j].GetComponent<PaintablePlane>().paintingID == paintingID){
					numPainted++;
				}
			}
		}
		
		return (float)numPainted / numPPlane;
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
