﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour {

	
	private Animator animator;		// 《Animator》コンポーネント用の変数
	
	public GameObject bullet; //GameObject型変数bulletを宣言
	public float bullet_speed = 10f;//float型変数speedを宣言。同時に10を代入
	
	public float attackingInterval = 5f;  //何秒おきに
	private float time = 0f;   //経過時間
	
	public int numFront = 1;
	public int numBorder = 1;
	
	public float speed = 1f;
	public float rotate_speed = 0.5f; // rotation x rotate_speed
	public int hp = 10;
	
	public int damagePoint = 1;
	
	public bool isMarkingTarget = false;
	
	public GameObject muzzle;
	public GameObject target;
	
	private bool isAttacking = false;
	public bool isDead = false;
	public float timeAttackMotion;
	public float timeDisappearingAfterDead;
	
	bool isDamage = false;
	
	[SerializeField] private AudioClip attackSound;
	private AudioSource audioSource;
	
	// Use this for initialization
	void Start () {
		
		animator = GetComponent< Animator >();	// 《Animator》コンポーネントの取得
		audioSource = GetComponent<AudioSource>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
		InitFlags();
		time += Time.deltaTime;
		
		if(isDead) {
			if(time >= timeDisappearingAfterDead){
				Destroy(this.gameObject);
			}
			return;
		}
		if(!isMarkingTarget){
			return;
		}
				
		if(isAttacking && time >= timeAttackMotion){
			Shot();
			isAttacking = false;
		}
		
		if(time >= attackingInterval){
			animator.SetBool("Attack", true);
			isAttacking = true;
			
			time = 0f;
		}
		
		Vector3 direction = Vector3.Normalize(target.transform.position - transform.position);
		//transform.Rotate(new Vector3(0, Vector3.Angle(direction, transform.TransformDirection(Vector3.forward)) * rotate_speed, 0));
		GetComponent<Rigidbody>().velocity = direction * speed;
		
	}
	
	private void InitFlags () {
	
		animator.SetBool("Attack", false);
		//animator.SetBool("Dead" , false);
	
	}
	
	// ダメージを受けた時の無敵処理
	IEnumerator invincibleTime(float time){
		
		isDamage = true;
		yield return new WaitForSeconds(time);		// 処理を待機.
		isDamage = false;
		
	}
	
	private void PlayAttackSound()
	{
		audioSource.clip = attackSound;
		audioSource.Play();
	}
	
	public void Damage (int deltaHP = 1) {
	
		hp -= deltaHP;
		
		if(hp <= 0){
			animator.SetBool("Dead" , true);
			isDead = true;
			time = 0f;
			GetComponent<RotateToPlayerWithAMaximumSpeed>().enabled = false;
			GetComponent<Rigidbody>().velocity = new Vector3(0, 0, 0);
			GetComponent<ItemDropController>().drop();
			GetComponent<Rigidbody>().detectCollisions = false;
		}
		
		StartCoroutine("invincibleTime" , 0.5f);
		
	}
	
	void Shot () {
		
		PlayAttackSound();
		
		for(int i = 0; i < numBorder; i++){
			
			Vector3 direction = (muzzle.transform.position - transform.position).normalized;
			
			float rotationRad = 20f * Mathf.Floor(i - numBorder / 2) * (float)Mathf.Deg2Rad;
			direction.x = direction.x * Mathf.Cos(rotationRad) - direction.z * Mathf.Sin(rotationRad);
			direction.z = direction.x * Mathf.Sin(rotationRad) + direction.z * Mathf.Cos(rotationRad);
			
			for(int j = 0; j < numFront; j++){
				
				float cur_speed = bullet_speed + 1.5f * j;
				
				GameObject bul = (GameObject)Instantiate(bullet, muzzle.transform.position, muzzle.transform.rotation);
				bul.GetComponent<Rigidbody>().velocity = direction * cur_speed;
				
			}
			
		}
		
	}
	
	void OnCollisionEnter(Collision collision){
	
		if(isDead) return;
		
		if(collision.transform.tag == "Bullet"){
			if(!isDamage){
				Damage();
			}		
		}
	
	}
	
}
