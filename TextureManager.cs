﻿using UnityEngine;
using System.Collections;

public class TextureManager : MonoBehaviour {

	public Texture[] PPlane;
	

	public Texture GetPaintablePlane(bool a, bool b, bool c, bool d){
	
		int index = 0;
		
		if(a) index += (1 << 1);
		if(b) index += (1 << 0);
		if(c) index += (1 << 3);
		if(d) index += (1 << 2);
	
		return PPlane[index];
		
	}

}
