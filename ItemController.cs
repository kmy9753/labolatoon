﻿using UnityEngine;
using System.Collections;

public class ItemController : MonoBehaviour {

	public int deltaFront = 0;
	public int deltaBorder = 0;
	
	public int deltaHP = 0;

	[SerializeField] private AudioClip gotSound;
	private AudioSource audioSource;

	// Use this for initialization
	void Start () {
	
		audioSource = GetComponent<AudioSource>();
	
	}
		
	// Update is called once per frame
	void Update () {
	
		transform.Rotate(new Vector3(0f, 40f * Time.deltaTime, 0f));
		
	}
	
	private void PlayGotSound()
	{
		audioSource.clip = gotSound;
		audioSource.Play();
	}
	
	IEnumerator got(){
		
		PlayGotSound();
		yield return new WaitForSeconds(gotSound.length);
		Destroy(gameObject);
		
	}
	
	void OnCollisionEnter (Collision collision) {
	
		if(collision.gameObject.tag == "Player"){
		
			GetComponent<Collider>().enabled = false;
			
			GunController gunController = collision.gameObject.transform.Find("MainCamera").Find("Gun").GetComponent<GunController>();
			HPManager hpManager = collision.gameObject.GetComponent<HPManager>();
		
			gunController.numFront  += deltaFront;
			gunController.numBorder += deltaBorder;
			
			hpManager.hp += deltaHP;
			
			StartCoroutine("got");
		}
	
	}
}
