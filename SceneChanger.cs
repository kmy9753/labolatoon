﻿using UnityEngine;
using System.Collections;

//using FCamera;

public class SceneChanger : MonoBehaviour {

	public GameObject textField;
	TimeManager timeManager;
	
	[SerializeField] private AudioClip endSound;
	
	private void PlayEndSound()
	{
		GetComponent<AudioSource>().clip = endSound;
		GetComponent<AudioSource>().Play();
	}
	
	// Use this for initialization
	void Start () {
	
		timeManager = textField.GetComponent<TimeManager>();
		
	}
	
	// Update is called once per frame
	void Update () {
		if(timeManager.IsEnded()){
			PlayEndSound();
		
			//this.GetComponent<CrossfadeSample>().LoadLevel("result");
		}
	}
	
}
