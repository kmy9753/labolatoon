﻿using UnityEngine;
using System.Collections;

public class GunController : MonoBehaviour {

	public GameObject bullet; //GameObject型変数bulletを宣言
	public float speed = 10f;//float型変数speedを宣言。同時に10を代入
	
	public float interval = 0.5f;  //何秒おきに
	private float time = 0f;   //経過時間
	
	public AudioClip SE;
	
	public int numFront = 1;
	public int numBorder = 1;


	// Update is called once per frame
	void Update () {
	
		time += Time.deltaTime; //タイムを加算
		
		if(Input.GetButton("Fire1")){
			
			//マウスが左クリックされた時
			if(time >= interval){
				
				//time（経過時間）がintervalを上回った
				Shot ();
				time = 0;   //初期化する
			}
		}
		
	}
	
	void Shot () {
		
		Transform muzzle = transform.Find("Lo_Muzzle");
		
		for(int i = 0; i < numBorder; i++){
			
			Vector3 direction = (muzzle.position - transform.position).normalized;
			
			float rotationRad = 10f * Mathf.Floor(i - numBorder / 2) * (float)Mathf.Deg2Rad;
			direction.x = direction.x * Mathf.Cos(rotationRad) - direction.z * Mathf.Sin(rotationRad);
			direction.z = direction.x * Mathf.Sin(rotationRad) + direction.z * Mathf.Cos(rotationRad);
			
			for(int j = 0; j < numFront; j++){
				
				float cur_speed = speed + 1.5f * j;
				
				GameObject bul = (GameObject)Instantiate(bullet, muzzle.position, muzzle.rotation);
				bul.GetComponent<Rigidbody>().velocity = direction * cur_speed;
				
			}
			
		}
		
		
		GetComponent<AudioSource>().PlayOneShot(SE);
		
	}
	
}


