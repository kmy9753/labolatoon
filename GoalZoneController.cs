﻿using UnityEngine;
using System.Collections;

public class GoalZoneController : MonoBehaviour {

	private bool isGoaled = false;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public bool IsGoaled () {
	
		return isGoaled;
	
	}
	
	void OnTriggerEnter (Collider collider) {
	
		if(collider.transform.tag == "Player"){
			isGoaled = true;
		}
	
	}
}
