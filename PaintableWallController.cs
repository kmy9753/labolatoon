﻿using UnityEngine;
using System.Collections;

public class PaintableWallController : MonoBehaviour {

	public bool hasLeft  = false;
	public bool hasFront = false;
	public bool hasRight = false;
	public bool hasBack  = false;

	// Use this for initialization
	void Start () {
	
		if(!hasLeft){
			GameObject.Destroy(transform.Find("LeftWall").gameObject);
			GameObject.Destroy(transform.Find("LeftPainting").gameObject);
		}
        if(!hasFront){
			GameObject.Destroy(transform.Find("FrontWall").gameObject);
			GameObject.Destroy(transform.Find("FrontPainting").gameObject);
        }
     	if(!hasRight){
			GameObject.Destroy(transform.Find("RightWall").gameObject);
			GameObject.Destroy(transform.Find("RightPainting").gameObject);
		}
        if(!hasBack){
			GameObject.Destroy(transform.Find("BackWall").gameObject);
			GameObject.	Destroy(transform.Find("BackPainting").gameObject);
        }

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
