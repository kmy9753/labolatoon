﻿using UnityEngine;
using System.Collections;

public class CameraFilter : MonoBehaviour {
	
	public Material mat;  // かけたい効果のマテリアル
	
	void OnRenderImage ( RenderTexture src, RenderTexture dest ) {
		Graphics.Blit(src, dest, mat);
	}
}