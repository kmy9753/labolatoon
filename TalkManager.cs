﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

// Quoted from http://tsubakit1.hateblo.jp/entry/2014/12/06/233000
// Edited

public class TalkManager : MonoBehaviour 
{
	public string[] scenarios;
	
	[SerializeField] Text uiText;
	[SerializeField] string serif;
	
	[SerializeField][Range(0.001f, 0.3f)]
	float intervalForCharacterDisplay = 0.05f;
	
	private string currentText = string.Empty;
	private float timeUntilDisplay = 0;
	private float timeElapsed = 1;
	private int currentLine = 0;
	private int lastUpdateCharacter = -1;

	public string nextScene = "stage_1";

	// 文字の表示が完了しているかどうか
	public bool IsCompleteDisplayText 
	{
		get { return  Time.time > timeElapsed + timeUntilDisplay; }
	}
	
	void Start()
	{
		scenarios = serif.Split (',');
		SetNextLine();
	}

	void switchScene () {

		//Destroy(GameObject.Find("Stage"));
		Application.LoadLevel (nextScene);

	}

	void Update () 
	{
		// 文字の表示が完了してるならクリック時に次の行を表示する
		if( IsCompleteDisplayText ){
			if(Input.GetMouseButtonDown(0)){
				if(currentLine < scenarios.Length){
					SetNextLine();
				} else if(currentLine >= scenarios.Length){
					switchScene();
				}
			}
			if(Input.GetKeyUp(KeyCode.Q)) 	{
				switchScene();
			}
		}else{
			// 完了してないなら文字をすべて表示する
			if(Input.GetMouseButtonDown(0)){
				timeUntilDisplay = 0;
			}
		}
		
		int displayCharacterCount = (int)(Mathf.Clamp01((Time.time - timeElapsed) / timeUntilDisplay) * currentText.Length);
		if( displayCharacterCount != lastUpdateCharacter ){
			uiText.text = currentText.Substring(0, displayCharacterCount);
			lastUpdateCharacter = displayCharacterCount;
		}
	}
	
	
	void SetNextLine()
	{
		currentText = scenarios[currentLine];
		timeUntilDisplay = currentText.Length * intervalForCharacterDisplay;
		timeElapsed = Time.time;
		currentLine ++;
		lastUpdateCharacter = -1;
	}
}