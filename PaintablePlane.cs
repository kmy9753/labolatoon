﻿using UnityEngine;
using System.Collections;

public class PaintablePlane : MonoBehaviour {

	public GameObject manager;
	
	public int row;
	public int col;
		
	public enum PaintingID { None = -1, Player = 0, Enemy = 1 };
	
	public PaintingID paintingID = PaintingID.None;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	//void OnCollisionEnter(Collision collision) {
	void OnTriggerEnter(Collider collider) {
	
		if(collider.tag == "Bullet") {
			manager.GetComponent<PaintablePlaneManager>().Paint(this.gameObject, PaintingID.Player);
		}
		if(collider.tag == "EnemyBullet") {
			manager.GetComponent<PaintablePlaneManager>().Paint(this.gameObject, PaintingID.Enemy);
		}
		
		if(collider.gameObject.tag == "Player") {
			collider.GetComponent<IkaController>().onPaint = (paintingID == PaintingID.Player);
		}
		
	}
}
