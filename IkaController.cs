﻿using UnityEngine;
using System.Collections;

using UnityStandardAssets.Characters.FirstPerson;

public class IkaController : MonoBehaviour {

	public bool onPaint = false;
	public bool isSwimming = false;
    public bool isRising = false;
	public GameObject Floor;
	
	public float interval = 0.5f;  //何秒おきに
	private float time = 0f;   //経過時間

    private Vector3 positionBeforeDiving;
	
	[SerializeField] private AudioClip enterWaterSound;
	[SerializeField] private AudioClip riseSound;
	
	private AudioSource audioSource;
	
	public Material cameraFilterMat;
	
	// Use this for initialization
	void Start () {
	
		audioSource = GetComponent<AudioSource>();
	
	}
	
	private void PlayEnterWaterSound()
	{
		audioSource.clip = enterWaterSound;
		audioSource.Play();
	}
	
	private void PlayRiseSound()
	{
		audioSource.clip = riseSound;
		audioSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
		
		if((isSwimming && !onPaint) && !isRising){
			Rise();
		}
		
		time += Time.deltaTime; //タイムを加算
	    
		if(!onPaint) return;
		
		if((Input.GetButton("Fire2") || Input.GetKeyDown(KeyCode.Q))){
			if(time < interval) return;
			
			time = 0f;
			
			if(!isSwimming){
				Swim();
			}
			
			else {
				Rise();
			}
		}
				
	}
	
	IEnumerator divingTime(float time){
		
		yield return new WaitForSeconds(time);		// 処理を待機.
		//GameObject camera = transform.Find("MainCamera");
		Camera.main.GetComponent<CameraFilter>().mat = cameraFilterMat;
		Camera.main.GetComponent<CameraFilter>().enabled = true;
		
	}
	
	IEnumerator risingTime(float time){
		
		yield return new WaitForSeconds(time);		// 処理を待機.
		transform.Find("MainCamera").GetComponent<CameraFilter>().enabled = false;
		
	}
	
	public void Swim(){
	
        positionBeforeDiving = transform.position;
        
        //GetComponent<Rigidbody>().detectCollisions = false;
		Floor.GetComponent<MeshCollider>().enabled = false;
//		transform.position -= new Vector3(0f, 0.1f, 0f);
		transform.Find("MainCamera").Find("Gun").gameObject.SetActive(false);
		
		//this.gameObject.GetComponent<FirstPersonController>().enabled = false;
		StartCoroutine("divingTime" , 0.35f);
		
		isSwimming = true;
		PlayEnterWaterSound();
		
	}
	
	public void Rise(){
	
		//transform.Translate(new Vector3(0f, 2.0f, 0f));
		isRising = true;
		
		print ("in Rise... : before y = " + positionBeforeDiving.y + ", now y = " + transform.position.y);
		iTween.MoveTo(this.gameObject, 
		              iTween.Hash("y", positionBeforeDiving.y,
		                          "time", 0.5f, "easetype", iTween.EaseType.linear,// "islocal", true, 
		                          "oncomplete", "CompleteHandler", "oncompletetarget", this.gameObject));
		
		StopCoroutine("divingTime");
		StartCoroutine("risingTime" , 0.15f);
		PlayRiseSound();
		
	}
	
	void OnCollisionEnter (Collision collision){
	
		print ("hit!!!");
		print (collision.transform.tag);
	
		//if(isRising && collision.transform.tag == "Building"){	
		//	iTween.Stop();
		//	StartCoroutine("divingTime" , 0.05f);
		//	Swim();
		//	
		//	isRising = false;
		//}
	
	}
	
	private void CompleteHandler () {
		
		print ("in CompleteHandler... y = " + transform.position.y);
				
		transform.position = new Vector3(transform.position.x, positionBeforeDiving.y, transform.position.z);

		//GetComponent<Rigidbody>().detectCollisions = true;
		Floor.GetComponent<MeshCollider>().enabled = true;
		transform.Find("MainCamera").Find("Gun").gameObject.SetActive(true);
		
		//this.gameObject.GetComponent<FirstPersonController>().enabled = true;
		//transform.Find("MainCamera").GetComponent<CameraFilter>().enabled = false;
		
		isSwimming = false;
		isRising = false;
		
		//iTween.Stop(this.gameObject, "move");
		
	}
	
}
