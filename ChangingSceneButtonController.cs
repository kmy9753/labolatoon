﻿using UnityEngine;
using System.Collections;
using FCamera;

public class ChangingSceneButtonController : MonoBehaviour {

	public string nextSceneName;
	public bool decideFromGameController;
	private CrossfadeSample gameController;
	
	[SerializeField] private AudioClip pushSound;
	private AudioSource audioSource;
	
	// Use this for initialization
	void Start () {
		
		audioSource = GetComponent<AudioSource>();
		
		if(decideFromGameController){
			gameController = GameObject.Find("GameController").GetComponent<CrossfadeSample>();
			
			switch(gameController.GetSceneName()){
				case "stage_1": nextSceneName = "story_2"; break;
				case "stage_2": nextSceneName = "story_3"; break;
				case "stage_3": nextSceneName = "epilogue"; break;
				default: nextSceneName = "title"; break;
			}
		}
		
	}
	
	IEnumerator changeScene(){
		PlayPushSound();
		yield return new WaitForSeconds(pushSound.length);		// 処理を待機.
		Application.LoadLevel(nextSceneName);
		
	}
	
	private void PlayPushSound()
	{
		audioSource.clip = pushSound;
		audioSource.Play();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	public void ButtonPush() {
	
		StartCoroutine("changeScene");
		
	}
}
