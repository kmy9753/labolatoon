﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class HPManager : MonoBehaviour {

	public int hp = 10;
	bool isDamage = false;
	
	public Image panel_flashMonitor;
	
	[SerializeField] private AudioClip damagedSound;
	private AudioSource audioSource;
	
	// Use this for initialization
	void Start () {
	
		audioSource = GetComponent<AudioSource>();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	// ダメージを受けた時の無敵処理
	IEnumerator invincibleTime(float time){
	
		isDamage = true;
		yield return new WaitForSeconds(time);		// 処理を待機.
		isDamage = false;
		
	}
	
	private void PlayDamagedSound()
	{
		audioSource.clip = damagedSound;
		audioSource.Play();
	}
	
	void Damage (int deltaHP = 1) {
	
		PlayDamagedSound();
	
		hp -= deltaHP;
		hp = Mathf.Max(hp, 0);
		StartCoroutine("monitorFlash");
		StartCoroutine("invincibleTime" , 0.5f);
	
	}
	
	public bool IsDead () {
		
		return hp <= 0;
		
	}
	
	void OnCollisionEnter (Collision collision) {
		
		if(collision.transform.tag == "EnemyBullet"){
			if(!isDamage) {
				Damage(collision.transform.GetComponent<BulletController>().damagePoint);
			}
		} 
		if(collision.transform.tag == "Enemy" && !collision.transform.GetComponent<EnemyController>().isDead){
			if(!isDamage) {
				Damage(collision.transform.GetComponent<EnemyController>().damagePoint);
			}
		}
	
	}
	
	IEnumerator monitorFlash(){
	
		panel_flashMonitor.enabled = true;
		yield return new WaitForSeconds(0.7f);		// 処理を待機.
		panel_flashMonitor.enabled = false;
		
	}
	
}
