# Labolatoon
明日に控えた学会を前に，研究データを大学に忘れてしまった幼馴染．  
「俺が取りに行ってやるよ．」悲しみに暮れる幼馴染に，あなたは冒険を誓う．  


プレイ画面
--------

|||
|---|---|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_title.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_story.jpg)|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_paint.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_dive.jpg)|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_barrier.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_barrier_break.jpg)|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_battle.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_result.jpg)|
|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_stage2.jpg)|![](https://dl.dropboxusercontent.com/u/67010278/screenshot/Labolatoon/ss_boss2.jpg)|