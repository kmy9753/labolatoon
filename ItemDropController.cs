﻿using UnityEngine;
using System.Collections;

public class ItemDropController : MonoBehaviour {

	public GameObject [] items;
	public float [] droppingProbability;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	int getItemIndex () {
	
		float probability = Random.Range(0f, 1f);
		int itemIndex = items.Length;		
		
		for(int i = 0; i < items.Length; i++){
			probability -= droppingProbability[i];
			
			if(probability < 0){
				itemIndex = i;
				break;
			}
		}
		
		return itemIndex;
		
	}
	
	public void drop () {
	
		int itemIndex = getItemIndex();
		if(itemIndex >= items.Length) return;
		
		GameObject item = (GameObject)Instantiate(items[itemIndex], transform.position + new Vector3(0f, 0.5f, 0f), Quaternion.identity);
		item.GetComponent<Rigidbody>().velocity = new Vector3(0f, 6f, 0f);
		
	}
}
