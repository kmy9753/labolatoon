﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TimeManager : MonoBehaviour {

	[SerializeField] private float defaultTime = 60f;
	[SerializeField] private float restTime;
	//public ballScript BallScript;
	
	void Start () {
	
		restTime = defaultTime;
	
		//初期値60を表示
		//float型からint型へCastし、String型に変換して表示
		GetComponent<Text>().text = ((int)restTime).ToString();
		
	}
	
	public int GetRestTime() {
	
		return (int)restTime;
	
	}
	
	public int GetDefaultTime() {
	
		return (int)defaultTime;
		
	}
	
	void Update (){
	
		//1秒に1ずつ減らしていく
		restTime -= Time.deltaTime;
		//マイナスは表示しない
		if (restTime < 0f) restTime = 0f;
		GetComponent<Text> ().text = ((int)restTime).ToString ();
		
	}
	
	public bool IsEnded(){
	
		return restTime < 1f;
	
	}
}