﻿using UnityEngine;
using System.Collections;

public class NoDestroied : MonoBehaviour {

	public string [] destroyableScenes;
	
	void Awake()
	{
	
		DontDestroyOnLoad(gameObject);
		
	}
	
	void Update()
	{
		
		string loadedScene = Application.loadedLevelName;
	
		for(int i = 0; i < destroyableScenes.Length; i++){
			if(destroyableScenes[i] == loadedScene){
				Destroy(gameObject);
				return;
			}
		}
		
	}
	
}
