using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using FCamera;

public class ResultManager : MonoBehaviour {

	[SerializeField] private AudioClip endSound;
	
	[SerializeField] private Text basicBonusText;
	[SerializeField] private Text timeBonusText;
	[SerializeField] private Text paintedBonusText;
	[SerializeField] private Text totalBonusText;
	
	private void PlayEndSound()
	{
		GetComponent<AudioSource>().clip = endSound;
		GetComponent<AudioSource>().Play();
	}


	Transform stage;
	CrossfadeSample gameController;

	int basicBonus;
	int timeBonus;
	int paintedBonus;

	string ConvertBasicBonusText(int basicBonus) {
		
		this.basicBonus = basicBonus;
	
		return "キホンボーナス：" + basicBonus;
	
	}
	
	string ConvertPaintedBonusText(double paintedRate) {
	
		paintedBonus = (int)(1000 * paintedRate);
	
		return "ヌラレボーナス：" + paintedBonus;
	
	}
	
	string ConvertTimeBonusText(int defaultTime, int restTime) {
	
		timeBonus = (int)(1000 * (((double)restTime / defaultTime)));
	
		return "タイムボーナス：" + timeBonus;
	
	}
	
	string ConvertTotalBonusText() {
	
		return "トータル：" + (basicBonus + timeBonus + paintedBonus);
	
	}

	// Use this for initialization
	void Start () {
		PlayEndSound();
		
		stage = GameObject.Find("Stage").transform;
		gameController = GameObject.Find("GameController").GetComponent<CrossfadeSample>();
		
		Text [] textField = { basicBonusText, timeBonusText, paintedBonusText, totalBonusText };
		string [] text = { ConvertBasicBonusText(gameController.GetBasicPoint()),
						   ConvertTimeBonusText(gameController.GetDefaultTime(), gameController.GetRestTime()), 
					       ConvertPaintedBonusText(gameController.GetPaintedRate()),
					       ConvertTotalBonusText() };
		
		
		for(int i = 0; i < textField.Length; i++){
			textField[i].text = text[i];
		}
		
	}
	
	// Update is called once per frame
	void Update () {

		stage.Rotate(new Vector3(0f, 30f * Time.deltaTime, 0f));
			
	}
}
