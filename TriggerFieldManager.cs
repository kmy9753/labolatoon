﻿using UnityEngine;
using System.Collections;

public class TriggerFieldManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void OnTriggerEnter (Collider collider) {
		
		if(collider.transform.tag == "Player"){
			transform.parent.GetComponent<EnemyController>().isMarkingTarget = true;
		}
	
	}	

	void OnTriggerExit (Collider collider) {
	
		if(collider.transform.tag == "Player"){
			transform.parent.GetComponent<EnemyController>().isMarkingTarget = false;
		}
		
	}	
			
}
