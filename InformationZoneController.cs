﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class InformationZoneController : MonoBehaviour {

	public Text textField;
	public string informationString;
	public Material mat;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	
	void OnTriggerEnter (Collider collider) {
		
		if(collider.transform.tag == "Player"){
			textField.text = informationString;
			Camera.main.GetComponent<CameraFilter>().enabled = true;
			Camera.main.GetComponent<CameraFilter>().mat = mat;
		}
	
	}
	
	void OnTriggerExit (Collider collider) {
		
		if(collider.transform.tag == "Player"){
			textField.text = "";
			Camera.main.GetComponent<CameraFilter>().enabled = false;
		}
		
	}
	
}
