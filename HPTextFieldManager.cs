﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HPTextFieldManager : MonoBehaviour {

	public GameObject target;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		GetComponent<Text>().text = "HP: " + target.GetComponent<HPManager>().hp;
		
	}
}
